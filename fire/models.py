from itertools import groupby
import re
from datetime import datetime, timedelta
import random
from email.utils import parseaddr

from pyramid.security import Allow, ALL_PERMISSIONS
from pyramid.httpexceptions import HTTPNotFound

from sqlalchemy.schema import Table, Column, ForeignKey, CheckConstraint
from sqlalchemy.types import (SchemaType, TypeDecorator, Integer, DateTime, Text, Enum, Boolean, PickleType, String)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.engine import Engine
from sqlalchemy import event

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    object_session
    )

from zope.sqlalchemy import ZopeTransactionExtension

from .security import (hash_pwd, verify_pwd, random_hex_string)
from .errors import InternalFireError

import logging
log = logging.getLogger(__name__)

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(), expire_on_commit=False))


# ENABLE FOREIGN KEY CONSTRAINTS
# SQLite supports FOREIGN KEY syntax when emitting CREATE statements for tables,
# however by default these constraints have no effect on the operation of the
# table. Need to turn it on for each connection!
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


def includeme(config):
    from sqlalchemy import engine_from_config
    engine = engine_from_config(config.registry.settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)


def model_resource(model, match=None, postparam=None):
    """Creates a resource factory, suitable for passing to Pyramid's
    add_route, that returns a model class (or 404 if not found).
    The id for the model class is taken from the URL routing matchdict
    if the match parameter is used, or from the POST data if the postaparm
    is given.
   
    Using such a factory makes Pyramid pass the model instance as the
    context parameter to the view, after consulting the instance's
    __acl__ property to see if the current user has permission to the object.
    """
    # We kind of rely on that SQLite doesn't care about types. A different db
    # would mean here we need to add some type conversion as well.
    if match:
        def model_resource_factory(request):
            return model.get_or_404(request.matchdict[match])
    elif postparam:
        def model_resource_factory(request):
            return model.get_or_404(request.POST.getone(postparam))
    else:
        raise ValueError("Must specify either a name from matchdict or request.POST to use as an id")
    return model_resource_factory


class Base(object):
    """A declarative base for SQLAlchemy models."""

    @classmethod
    def get(cls, key):
        return DBSession().query(cls).get(key)

    @classmethod
    def get_or_404(cls, key):
        obj = cls.get(key)
        if obj is None:
            raise HTTPNotFound("No such %s : %s" % (cls.__name__, str(key)))
        return obj

    @classmethod
    def get_where(cls, *args, **kwargs):
        return DBSession().query(cls).filter(*args, **kwargs).all()

    @classmethod
    def get_all(cls, *args, **kwargs):
        return DBSession().query(cls).order_by(*args, **kwargs).all()

    @classmethod
    def query(cls):
        return DBSession().query(cls)

Base = declarative_base(cls=Base)


# From Zzzeek's Enum recipe {{{
# http://techspot.zzzeek.org/2011/01/14/the-enum-recipe/

class EnumSymbol(object):
    """Define a fixed symbol tied to a parent class."""

    def __init__(self, cls_, name, value, description):
        self.cls_ = cls_
        self.name = name
        self.value = value
        self.description = description

    def __reduce__(self):
        """Allow unpickling to return the symbol 
        linked to the DeclEnum class."""
        return getattr, (self.cls_, self.name)

    def __iter__(self):
        return iter([self.value, self.description])

    def __repr__(self):
        return "<%s>" % self.name


class EnumMeta(type):
    """Generate new DeclEnum classes."""

    def __init__(cls, classname, bases, dict_):
        cls._reg = reg = cls._reg.copy()
        for k, v in dict_.items():
            if isinstance(v, tuple):
                sym = reg[v[0]] = EnumSymbol(cls, k, *v)
                setattr(cls, k, sym)
        return type.__init__(cls, classname, bases, dict_)

    def __iter__(cls):
        return iter(cls._reg.values())


class DeclEnum(object):
    """Declarative enumeration."""

    __metaclass__ = EnumMeta
    _reg = {}

    @classmethod
    def from_string(cls, value):
        try:
            return cls._reg[value]
        except KeyError:
            raise ValueError("Invalid value for %r: %r" % (cls.__name__, value))

    @classmethod
    def values(cls):
        return cls._reg.keys()

    @classmethod
    def db_type(cls):
        return DeclEnumType(cls)


class DeclEnumType(SchemaType, TypeDecorator):
    def __init__(self, enum):
        self.enum = enum
        self.impl = Enum(*enum.values(),
                         name="ck%s" % re.sub('([A-Z])', lambda m: "_" + m.group(1).lower(), enum.__name__))

    def _set_table(self, table, column):
        self.impl._set_table(table, column)

    def copy(self):
        return DeclEnumType(self.enum)

    def process_bind_param(self, value, dialect):
        if value is None:
            return None
        return value.value

    def process_result_value(self, value, dialect):
        if value is None:
            return None
        return self.enum.from_string(value.strip())

# End Enum recipe }}}


class Settings(Base):
    """Course wide settings.
    """
    __tablename__ = 'settings'

    id = Column(Text, primary_key = True)
    value = Column(PickleType)

    _defaults = dict(course_name='Default course',
                     course_url='')

    _cache = dict()

    # This class deviates from base, as other code should not
    # deal directly with instances
    @classmethod
    def get(cls, key):
        if key not in cls._defaults:
            raise InternalFireError("Invalid setting name")
        if key in cls._cache:
            return cls._cache[key]
        obj = DBSession().query(cls).get(key)
        if obj is None:
            return cls._defaults[key]
        else:
            cls._cache[key] = obj.value
            return obj.value
    
    @classmethod
    def set(cls, key, value):
        if key not in cls._defaults:
            raise InternalFireError("Invalid setting name")
        cls._cache.pop(key, None)
        obj = DBSession().query(cls).get(key)
        if obj is None:
            obj = cls(id=key)
            DBSession().add(obj)
        obj.value = value


class CourseCode(Base):
    __tablename__ = "coursecodes"
    code = Column(String(10), CheckConstraint("code <> ''"), primary_key=True)


class SubmitterMixin(object):
    """A mixin for Users and Groups. Includes common functionality for
    submitters."""

    # TODO: perhaps these are better kept in the Lab class. Refactor?
    
    def submissions_for_lab(self, lab):
        """Returns all submissions for a given lab for this user or group."""
        lab_subs = [sub for sub in self.submissions if sub.lab_id == lab.id]
        lab_subs.sort(key=lambda s: s.number)
        return lab_subs

    def get_pending_submission(self, lab):
        """Returns the pending submission for a lab, if one exists, None otherwise."""
        subs = [sub for sub in self.submissions_for_lab(lab)
                if sub.status == Submission.Statuses.submitted]
        if len(subs) > 0:
            assert len(subs) == 1  # Should be maintained always
            return subs[0]
        else:
            return None

    def has_new_submission(self, lab):
        return any(s.status is Submission.Statuses.new for s in self.submissions_for_lab(lab))

    def has_active_submissions(self, lab):
        return any(s.active for s in self.submissions_for_lab(lab))


class EmailVerification(Base):
    """ Allow to verify an email when users register"""
    __tablename__ = 'emailverifications'
    email = Column(Text)
    token = Column(Text,
                   primary_key=True,
                   default=lambda: random_hex_string(16))
    cdate = Column(DateTime,
                   default=datetime.now)

    @staticmethod
    def create(dbsession=DBSession, **kwargs):
        verif = EmailVerification(**kwargs)
        dbsession.add(verif)
        dbsession.flush()
        return verif

    @classmethod
    def check(cls, token):
        """ Checks a token: returns the email address for a valid token
        or raise an exception """
        verif = DBSession.query(cls).get(token)
        if verif is None:
            raise ValueError('Invalid verification token')
        else:
          return verif.email

    @classmethod
    def get_unverified(cls):
        """ Returns rows that don't correspond to a user account """
        verified = DBSession().query(User.email).subquery()
        return DBSession.query(cls).filter(~cls.email.in_(verified)).all()


class PasswordReset(Base):
    """Allows to reset users password"""
    __tablename__ = 'passwordresets'
    email = Column(Text, ForeignKey('users.email'))
    token = Column(Text,
                   primary_key=True,
                   default=lambda: random_hex_string(16))
    cdate = Column(DateTime,
                   default=datetime.now)

    @staticmethod
    def create(dbsession=DBSession, **kwargs):
        reset = PasswordReset(**kwargs)
        dbsession.add(reset)
        dbsession.flush()
        return reset

    @classmethod
    def check(cls, token):
        """ Checks a token: returns the email address for a valid token
        or raise an exception """
        reset = DBSession.query(cls).get(token)
        if reset is None:
            raise ValueError('Invalid password reset token.')
        else:
            return reset.email


class User(Base, SubmitterMixin):
    """A user in the system (any kind; students, teachers, admins)
    """
    __tablename__ = 'users'

    class Roles(DeclEnum):
        student = 'student', 'Student'
        grader  = 'grader',  'Grader'
        admin   = 'admin',   'Course Admin'

    email      = Column(Text, primary_key=True)
    _pw_hash   = Column('pw_hash', Text) 
    first_name = Column(Text)
    last_name  = Column(Text)
    last_login = Column(DateTime)                        #: date last logged in
    role       = Column(String(50), nullable=False)

    __mapper_args__ = { 'polymorphic_on': role }

    @property
    def id(self):
        """ ID property, created for backward compatibility"""
        return self.email

    def __init__(self, id_, **kwargs):
        """
        Initialize a User using a valid mail recipient.
        For instance, ``User("test@test.com")`` will set the user email to
        "test@test.com" but ``User("Bob Bobsson <bob@test.com>")`` will also set the
        user first and last names.
        """
        super(User, self).__init__(**kwargs)
        name, address = parseaddr(id_)
        self.email = address
        try:
          names = name.split(' ', 1)
          self.first_name = self.first_name or names.pop(0)
          self.last_name  = self.last_name or names.pop(0)
        except IndexError:
          pass

    @classmethod
    def create(cls, *args, **kargs):
        u = cls(*args, **kargs)
        DBSession.add(u)
        DBSession.flush()

    @property
    def __acl__(self):
        return [(Allow, 'role:admin', ALL_PERMISSIONS)]

    @hybrid_property
    def password(self):
        """Always an empty string, only used for setting a password."""
        # Note: As a convention, do not handle _pw_hash outside this class.
        # It contains a salted hash, so leaking it opens up the possibility
        # of rainbow attacks.
        return ""

    @password.setter
    def password(self, value):
        """Updates the stored hashed password, actual value is not stored."""
        self._pw_hash = hash_pwd(value)

    @classmethod
    def check_password(cls, userid, pwd):
        """Returns true if ``pwd`` matches the stored hash of the users password.
        Returns false if the user is not verified.
        """
        u = cls.get(userid)
        log.debug('checking_password: for %r', u)
        if u is not None:
            return verify_pwd(pwd, u._pw_hash)
        return False

    @property
    def name(self):
        if self.first_name and self.last_name:
            return "%s %s" % (self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        elif self.last_name:
            return self.last_name
        else:
            return None

    @property
    def safe_info(self):
        """Returns a dictionary of information about the user that is safe to display."""
        info = dict()
        for prop in ['id', 'email', 'name', 'id_number', 'course', 'role']:
            info[prop] = getattr(self, prop, None)
        return info

    @property
    def active_membership(self):
        ag = [m for m in self.memberships if m.active]
        assert len(ag) <= 1, "Multiple active groups for user."
        if len(ag):
            return ag[0]
        else:
            return None

    def __repr__(self):
        return "<User %s>" % self.email

    def __unicode__(self):
        return u"%s <%s>" % (self.name, self.email)


class Student(User):
    __tablename__ = 'students'
    __mapper_args__ = {'polymorphic_identity': 'student'}
    email = Column(Text, ForeignKey('users.email'), primary_key=True)
    id_number  = Column(Text, unique=True)                        #: student id number
    course     = Column(String(10), ForeignKey('coursecodes.code', ondelete='restrict'))
    group_id = Column(Integer, ForeignKey('groups.id'))
    signup     = Column(DateTime, default=datetime.now)  #: date signed up

    __acl__ = [(Allow, 'role:grader', 'view'),
               (Allow, 'role:grader', 'edit'),
               (Allow, 'role:grader', 'impersonate'),
               (Allow, 'role:admin', ALL_PERMISSIONS)]

    def stat(self):
        return {}

    def can_submit(self, lab):
        if lab.individual:
            return True
        else:
            submissions = [s for s in self.submissions_for_lab(lab) if s.status != Submission.Statuses.new]
            return all(s.group_id == self.group_id for s in submissions)


class Grader(User):
    __tablename__ = 'graders'
    __mapper_args__ = {'polymorphic_identity': 'grader'}
    email = Column(Text, ForeignKey('users.email'), primary_key=True)

    __acl__ = [(Allow, 'role:admin', ALL_PERMISSIONS)]

    def stat(self):
        pending_submissions = Submission.query().filter_by(_status=Submission.Statuses.submitted, grader_id=self.email).count()
        return {'pending_submissions': pending_submissions}

_joincodeletters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ'


def mkjoincode():
    groups = Group.get_all()
    existing_codes = set(g.joincode for g in groups)
    for i in xrange(512):
        code = ''.join(random.choice(_joincodeletters) for i in xrange(6))
        if code not in existing_codes:
            return code
    # Probability of reaching here: 1/(Infinity - 1)
    raise InternalFireError("Failed to generate a joincode.")


class Group(Base, SubmitterMixin):
    __tablename__ = 'groups'

    id       = Column(Integer, primary_key=True)
    created  = Column(DateTime, default=datetime.now)
    joincode = Column(Text, nullable=False, default=mkjoincode)

    members = relationship("Student", backref="group")

    def __init__(self, owner):
        self.members.append(owner)

    # For convenience, read-only
    # active_members = relationship(User, secondary='group_memberships',
    #        secondaryjoin = and_(Membership.user_id == User.id, Membership.active == True),
    #        viewonly=True)

    @property
    def active_members(self):
        return [u for u in self.members]

    def _refresh(self):
        s = object_session(self)
        if s is not None:
            s.refresh(self)

    def add_user(self, user):
        if user not in self.active_members:
            self.members.append(user)

        # add the new submitter to the new submissions of the group
        for lab in Lab.get_active():
            for submission in self.submissions_for_lab(lab):
                if submission.status == Submission.Statuses.new:
                    if user not in submission.submitters:
                        submission.submitters.append(user)

    def remove_user(self, user):
        if user not in self.active_members:
            raise ValueError("Group.remove_user called with a non-member.")
        self.members.remove(user)
        # self._refresh()  # To update active_members property

    # -1 : not enough members
    #  0 : acceptable number of members
    #  1 : too many members
    def enough_members(self, lab):
        members = len(self.active_members)

        if members < lab.min_members:
            return -1

        if members > lab.max_members:
            return 1

        return 0

    def __repr__(self):
        return "<Group %d>" % self.id

    def __unicode__(self):
        return u"Group %d" % self.id


class Lab(Base):
    __tablename__ = 'labs'

    id = Column(Integer, primary_key=True)
    title = Column(Text)
    slug = Column(Text)
    description = Column(Text)
    predecessor = Column(Integer, ForeignKey('labs.id'))
    created = Column(DateTime, default=datetime.now)
    order = Column(Integer, default=0)
    active = Column(Boolean, default=True)
    individual = Column(Boolean, nullable=False)
    allow_physical_submissions = Column(Boolean, default=False, nullable=False)
    first_deadline = Column(DateTime)
    final_deadline = Column(DateTime)

    __mapper_args__ = {'polymorphic_on': individual,
                       'polymorphic_identity': True}

    @property
    def grader_assignment(self):
        return {a.grader.id: a.weight for a in self.graders}

    @classmethod
    def get_active(cls):
        return cls.query().filter(Lab.active == True).order_by(Lab.order).all()

    def summary_for_user(self, user):
        """Returns a dict containing the keys:
           - status : The general submission status. Ignores cancelled
               submissions, and new submissions unless it's the only one.
           - deciding_submission : the submission that decided the status
               above, e.g. the latest accepted or rejected submission,
               or the latest non-cancelled submission if none are accepted
               or rejected.
           - first_deadline : The effective first_deadline (may be overridden)
           - final_deadline : The effective final_dealine
           - first_deadline_override : True iif first_deadline is not the default deadline.
           - final_deadline_override : True iif final_deadline is not the default deadline.
        """
        subs = user.submissions_for_lab(self)

        first_deadline_override = False
        final_deadline_override = False

        if len(subs):
            # Deadline overrides always come from the latest submission,
            # regardless of their status.
            if subs[-1].first_deadline_override is not None:
                first_deadline = subs[-1].first_deadline_override
                first_deadline_override = True
            else:
                first_deadline = self.first_deadline
            
            if subs[-1].final_deadline_override is not None:
                final_deadline = subs[-1].final_deadline_override
                final_deadline_override = True
            else:
                final_deadline = self.final_deadline

            accepted_or_rejected = [s for s in subs
                    if s.status in (Submission.Statuses.accepted,
                                    Submission.Statuses.rejected,
                                    Submission.Statuses.submitted)]
            if len(accepted_or_rejected):
                deciding_submission = accepted_or_rejected[-1]
                status = deciding_submission.status
            else:
                not_new = [s for s in subs if s.status != Submission.Statuses.new]
                if not_new:
                    status = Submission.Statuses.cancelled
                    deciding_submission = not_new[-1]
                else:
                    status = Submission.Statuses.new
                    deciding_submission = None
        else:
            first_deadline = self.first_deadline
            final_deadline = self.final_deadline
            status = Submission.Statuses.new
            deciding_submission = None

        # Single deadline-labs have only a final deadline:
        if first_deadline is None:
            first_deadline = final_deadline

        return dict(first_deadline=first_deadline,
                    final_deadline=final_deadline,
                    first_deadline_override=first_deadline_override,
                    final_deadline_override=final_deadline_override,
                    status=status,
                    deciding_submission=deciding_submission)

    def pending_submissions(self):
        pending = [sub for sub in self.submissions if sub.status == Submission.Statuses.submitted]
        pending.sort(key=lambda s: s.submitted_date)
        return pending

    def last_submissions(self, status=None):
        get_submitter = lambda s: s.group_id or s.submitters

        last_submissions = []

        # Filter out submissions that are not the submitter's
        # latest attempt to pass the lab
        unsubmitted_statuses = [Submission.Statuses.new, Submission.Statuses.cancelled]
        active_submissions = [s for s in self.submissions if s.status not in unsubmitted_statuses]

        # Group submissions by submitter
        ordered_submissions = sorted(active_submissions, key=get_submitter)
        grouped_submissions = groupby(ordered_submissions, key=get_submitter)

        for submitter, submissions in grouped_submissions:
            latest_submission = max(submissions, key=lambda s: s.number)
            if status is None or latest_submission.status == status:
                last_submissions.append(latest_submission)

        if status in (Submission.Statuses.accepted, Submission.Statuses.rejected):
            last_submissions.sort(key=lambda s: s.reviewed_date)
        return last_submissions

    def rejected_submissions(self):
        return self.last_submissions(Submission.Statuses.rejected)

    def accepted_submissions(self):
        return self.last_submissions(Submission.Statuses.accepted)

    def __repr__(self):
        return '<Lab: %s>' % self.title


class GroupLab(Lab):
    __tablename__ = 'grouplabs'

    id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    max_members = Column(Integer,
                         CheckConstraint('max_members > 1'),
                         CheckConstraint('max_members >= min_members'),
                         default=2)
    min_members = Column(Integer, CheckConstraint('min_members > 0'), default=1)

    __mapper_args__ = {'polymorphic_identity': False}


class GraderAssignment(Base):
    """ Represent the fact that a gradder is assigned to a lab. The weight
    is used when a new submission comes in to randomly assign a grader to
    that submission, it allow precise control of the submission distribution.
    """
    __tablename__ = 'grader_assignments'

    lab_id = Column(Integer, ForeignKey('labs.id'), primary_key=True)
    grader_id = Column(Text, ForeignKey('graders.email'), primary_key=True)
    weight = Column(Integer, CheckConstraint('weight > 0'),
                    nullable=False, default=1)

    lab = relationship('Lab', backref='graders')
    grader = relationship('Grader')

    def __init__(self, lab_id, grader_id, weight):
        self.lab_id = lab_id
        self.grader_id = grader_id
        self.weight = weight

submitters_table = Table('submitters', Base.metadata,
                         Column('submission_id', Integer, ForeignKey('submissions.id')),
                         Column('user_id', Text, ForeignKey('users.email')))


class Submission(Base):
    __tablename__ = 'submissions'

    class Statuses(DeclEnum):
        new       = 'new',       'Not submitted'
        submitted = 'submitted', 'Submitted'
        cancelled = 'cancelled', 'Withdrawn'
        accepted  = 'accepted',  'Accepted'
        rejected  = 'rejected',  'Rejected'

    # List of "active" statuses. See the active property below for explanation.
    _active_statuses = (Statuses.new,
                        Statuses.submitted,
                        Statuses.accepted)

    id = Column(Integer, primary_key=True)
    lab_id = Column(Integer, ForeignKey('labs.id'), nullable=False)
    group_id = Column(Integer, ForeignKey('groups.id'))
    number = Column(Integer, nullable=False)
    _status = Column(Statuses.db_type(), nullable=False)
    first_deadline_override = Column(DateTime)
    final_deadline_override = Column(DateTime)
    grader_id = Column(Text, ForeignKey('graders.email'))

    submitted_date = Column(DateTime)  # Date of submission
    reviewed_date = Column(DateTime)   # Date of approval / rejection

    physical_submission = Column(Boolean)
    physical_submission_received_by_id = Column(Text, ForeignKey('graders.email'))
    physical_submission_received_date = Column(DateTime)

    comment = Column(Text)

    grade = Column(Text)
    note = Column(Text)
    review = Column(Text)

    lab = relationship(Lab, backref='submissions')
    group = relationship(Group, backref='submissions', lazy=False)
    grader = relationship(Grader, backref='grader_submissions',
                          primaryjoin=grader_id == Grader.email)
    physical_submission_received_by = relationship(Grader,
                                                   primaryjoin=physical_submission_received_by_id == Grader.email)
    submitters = relationship(Student, secondary=submitters_table, backref='submissions')

    def __init__(self, lab, group_or_submitter):
        if lab.individual and not isinstance(group_or_submitter, Student):
            raise InternalFireError("Tried creating a group submission on an individual lab.")
        elif not lab.individual and not isinstance(group_or_submitter, Group):
            if group_or_submitter.group is not None:
                group_or_submitter = group_or_submitter.group
            else:
                raise InternalFireError("Tried creating an individual submission on a group lab.")

        previous_submissions = group_or_submitter.submissions_for_lab(lab)

        # These non-null properties must be set before .lab and .group,
        # to avoid db integrity errors when the latter generate inserts (to obtain an id)
        # Note that .status is set below to trigger a check for double-submissions, so
        # it is important to set a *non-active* status here.
        self._status = Submission.Statuses.cancelled
        self.number = len(previous_submissions) + 1

        self.lab = lab
        if lab.individual:
            assert isinstance(group_or_submitter, Student)
            self.submitters = [group_or_submitter]
            self.group = None
        else:
            assert isinstance(group_or_submitter, Group)
            self.group = group_or_submitter
            # TODO: The following line is only needed for the grader to be able
            # to see new submissions in the list of a student's submissions. Needs
            # to be removed once deadline extensions are refactored into a separate entity
            self.submitters = group_or_submitter.active_members[:]

        if self.number > 1:
            # If the last submission for this group/user had a deadline
            # override, then copy it to the new one.
            previous_submission = next(sub for sub in previous_submissions if sub.number == self.number - 1)
            self.first_deadline_override = previous_submission.first_deadline_override  # may just be None
            self.final_deadline_override = previous_submission.final_deadline_override

        # The grader is chosen during actual submission
        self.grader = None

        # Now set the status correctly through the setter, to catch the case where
        # a student already has an active submission on this lab.
        self.status = Submission.Statuses.new

    @property
    def active(self):
        """An active submission is or will may be counted towards a grade.
        Thus, each student can only have one active submission per lab."""
        return self._status in Submission._active_statuses

    @property
    def status(self):
        return self._status

    @status.setter
    def status(self, newstatus):
        oldstatus = self._status
        # TODO: this invariant should not be checked here anymore, since we
        # allow incomplete groups to submit. However, I'm not entirely sure what the
        # behaviour in this case should be, gotta think about it more
        #if ( oldstatus not in Submission._active_statuses
        #        and newstatus in Submission._active_statuses):
            # Need to make sure none of the submitting students have
            # other active submissions on this lab.
            #all_active_submissions = [s for s in self.lab.submissions if s.active and s is not self]
            #users_with_active_subs = [user.email for sub in all_active_submissions
            #                                  for user in sub.submitters]
            #overlap = set(u.email for u in self.submitters) & set(users_with_active_subs)
            #if len(overlap):
            #    raise DataInvariantException("The following users already have submissions to this lab: %s"
            #            % ','.join(map(str, overlap)))
        if newstatus is Submission.Statuses.submitted:
            self.submitted_date = datetime.now()
            self.submitters = self.valid_submitters
        elif newstatus in (Submission.Statuses.rejected, Submission.Statuses.accepted):
            self.reviewed_date = datetime.now()
        elif newstatus is Submission.Statuses.cancelled:
            if oldstatus in [Submission.Statuses.rejected, Submission.Statuses.accepted]:
                raise ValueError("Impossible to withdraw a reviewed submission")
        self._status = newstatus
        # TODO: add logging

    @property
    def valid_submitters(self):
        submitters = self.submitters if self.lab.individual else self.group.active_members[:]
        return [s for s in submitters if s.can_submit(self.lab)]

    def submit(self):
        """
        This method needs to be called for every submit, do /not/ set the status
        field manually to Statuses.submitted from elsewhere in the code, as this
        function assigns a grader to the submission.
        """
        self.grader = self.choose_grader()
        self.status = self.Statuses.submitted

    def choose_grader(self):
        submitter = self.group if self.group is not None else self.submitters[0]

        # If this is not the first submission in the thread, reuse the grader
        if self.number > 1:
            previous_submissions = submitter.submissions_for_lab(self.lab)
            previous_submission = next(sub for sub in previous_submissions if sub.number == self.number - 1)
            if previous_submission.grader is not None:
                return previous_submission.grader

        # If the lab has a predecessor, reassign to its grader
        if self.lab.predecessor is not None:
            predecessor = Lab(id=self.lab.predecessor)
            predecessor_submissions = submitter.submissions_for_lab(predecessor)

            if predecessor_submissions:
                return predecessor_submissions[0].grader

        # Otherwise, assign a random grader
        weights = self.lab.grader_assignment.items()
        if not weights:
            raise InternalFireError("Misconfiguration: No grader weights are set on lab %d." % self.lab.id)

        # Chosen grader is the one with lowest value for
        #   ("nr assigned submission" + 1) / "weight"
        # i.e., who would have the lowest current work load if that person
        # was assigned this submission.
        last_submissions = self.lab.last_submissions()
        assigned = lambda grader_id: len(filter(lambda s: s.grader.id == grader_id, last_submissions))
        choice, _ = min(weights, key=lambda (grader_id, weight): (assigned(grader_id) + 1) / float(weight))
        log.debug("Selected lowest weight grader: %s", choice)
        grader = Grader.get(choice)

        if grader is None:
            raise InternalFireError("Misconfiguration: Grader %s does not exist or isn't a grader." % choice)

        return grader

    def can_be_withdrawn(self):
        if self.status != Submission.Statuses.submitted:
            return False

        # TODO: This should be well thought through

        first_deadline = self.first_deadline_override or self.lab.first_deadline
        final_deadline = self.final_deadline_override or self.lab.final_deadline

        # No withdrawals are allowed after the final deadline
        if final_deadline and datetime.now() > final_deadline:
            return False

        # Withdrawals after the first deadline are only allowed
        # if one of the previous submissions was already reviewed
        if first_deadline and final_deadline and first_deadline < datetime.now() <= final_deadline:
            previous_submissions = [sub for sub in self.lab.submissions if sub.number < self.number]
            return any(sub.status == Submission.Statuses.rejected for sub in previous_submissions)

        # Withdrawals are allowed before the first deadline
        return True

    def withdraw(self):
        """
        This method is a shorctut to set the submission status to cancelled
        """
        self.status = self.Statuses.cancelled

    def post_review(self, accepted, grade, note, review):
        self.status = Submission.Statuses.accepted if accepted else Submission.Statuses.rejected
        self.reviewed_date = datetime.now()

        self.note = note
        self.grade = grade
        self.review = review


class FileCap(Base):
    __tablename__ = 'filecaps'

    id = Column(Text, primary_key=True)
    is_archive = Column(Boolean, nullable=False, default=False)
    filename = Column(Text, nullable=False)
    contents = Column(PickleType, nullable=False)
    issued = Column(DateTime, nullable=False, default=datetime.now)
    expires = Column(DateTime, nullable=False)
    ip_address = Column(Text)

    def __init__(self, ttl=60):
        self.id = random_hex_string(32)
        self.issued = datetime.now()
        self.expires = self.issued + timedelta(seconds=ttl)

    @classmethod
    def issue_single(cls, path, name, ttl=60):
        """Issues a filecap for a single file. The path to the file
        is stored in the contents field."""
        fc = cls(ttl)
        fc.is_archive = False
        fc.filename = name
        fc.contents = path
        DBSession().add(fc)
        return fc

    @classmethod
    def issue_archive(cls, paths, name, ttl=60):
        """Issues a filecap for an archive of files (created on download).
        The contents field will be set to a list of pairs, each containing
        a full fileystem path to a file as well as the path of that file
        within the archive."""
        fc = cls(ttl)
        fc.is_archive = True
        fc.filename = name
        fc.contents = paths[:]
        DBSession().add(fc)
        return fc
