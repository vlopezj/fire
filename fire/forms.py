import datetime

from genshi.template import TemplateLoader
from pyramid.asset import abspath_from_asset_spec
from pyramid.threadlocal import get_current_request

import colander as c

from . import templating
from .schemata import *


def genshi_deform_renderer(tmpl_name, **kw):
    loader = TemplateLoader()
    path = abspath_from_asset_spec('fire:templates/form/' + tmpl_name + '.html')
    tmpl = loader.load(path)
    kw['utils'] = templating._renderer_utilities
    return tmpl.generate(**kw)


def make_form(schema, *args, **kwargs):
    if 'renderer' not in kwargs:
        kwargs['renderer'] = genshi_deform_renderer
    if 'csrf_token' not in kwargs:
        request = get_current_request()
        kwargs['csrf_token'] = request.session.get_csrf_token()
    form = deform.Form(schema, *args, **kwargs)
    return form


def make_form_message(msg, title=None, type='info', **kw):
    return genshi_deform_renderer('message', type=type, title=title, msg=msg, **kw)


def _personnummer_prepare(id_number):
    if not id_number:
        return id_number
    id_number = id_number.replace('-', '')
    if len(id_number) == 12 and id_number.startswith('19'):
        id_number = id_number[2:]
    return id_number

# class SignupSchema(c.MappingSchema):
# 
#     email = c.SchemaNode(c.String(), 
#             validator=c.Email(),
#             missing = None,
#             title='email address')
# 
#     first_name = c.SchemaNode(c.String(), 
#             validator=c.Length(min=1), 
#             missing = None,
#             title='first name')
# 
#     last_name = c.SchemaNode(c.String(), 
#             validator=c.Length(min=1), 
#             title='last name')
# 
#     id_number = c.SchemaNode(c.String(),
#             preparer=_personnummer_prepare,
#             validator=c.Regex(r'^[0-9a-zA-Z]{10}$', msg='Must be in the format YYMMDDNNNN'),
#             title='id number (personnumer)')
# 
#     password = c.SchemaNode(c.String(), 
#             validator=c.Length(min=5), 
#             widget=deform.widget.PasswordWidget(),
#             title='password')
#     confirm = c.SchemaNode(c.String(), 
#             widget=deform.widget.PasswordWidget(),
#             title='password again')
# 
#     _codes = [cc.code for cc in CourseCode.get_all()]
#     course = c.SchemaNode(c.String(), 
#             validator=c.OneOf(_codes),
#             widget=deform.widget.SelectWidget(values=zip(_codes,_codes)),
#             title='Course code')
# 
#     def validator(self, node, cstruct):
#         if User.get(cstruct["email"]):
#             exc = c.Invalid(node)
#             exc['email'] = 'A user with this email already exists'
#             raise exc
# 
#         if cstruct['password'] != cstruct['confirm']:
#             exc = c.Invalid(node)
#             exc['confirm'] = 'Must match password above.'
#             raise exc


class GraderSettingsSchema(c.MappingSchema):
    first_name = c.SchemaNode(c.String(), 
                              validator=c.Length(min=1),
                              missing=None,
                              title='First name')

    last_name = c.SchemaNode(c.String(), 
                             validator=c.Length(min=1),
                             title='Last name')


class PasswordSchema(c.MappingSchema):
    # old_password = c.SchemaNode(c.String(),
    #         widget=deform.widget.PasswordWidget(),
    #         title='Old password')

    password = c.SchemaNode(c.String(), 
                            validator=c.Length(min=12),
                            widget=deform.widget.PasswordWidget(),
                            title='New password')
    confirm = c.SchemaNode(c.String(), 
                           widget=deform.widget.PasswordWidget(),
                           title='Confirm new password')

    def validator(self, node, cstruct):
        if cstruct['password'] != cstruct['confirm']:
            exc = c.Invalid(node)
            exc['confirm'] = 'The passwords do not match.'
            raise exc


class DateTime(c.SchemaType):
    """A DateTime type that uses the YYYY-MM-DDTHH:MM format"""

    def serialize(self, node, appstruct):
        if not appstruct:
            return c.null

        if not isinstance(appstruct, datetime.datetime):
            raise c.Invalid(node, c._('"${val}" is not a datetime object',
                            mapping={'val': appstruct}))

        return appstruct.strftime("%Y-%m-%dT%H:%M")

    def deserialize(self, node, cstruct):
        if not cstruct:
            return c.null

        try:
            result = datetime.datetime.strptime(
                cstruct, "%Y-%m-%dT%H:%M")
        except ValueError as e:
            raise c.Invalid(node, c._("Invalid date: ${err}", mapping={'val': cstruct, 'err': e}))
        return result


class DateTimeInput(deform.widget.Widget):
    """A datetime widget for the DateTime type, using the 
    Bootstrap datetime picker. 
    (see fire/templates/form/datetimeinput.html)"""
    template = 'datetimeinput'
    type_name = 'datetime'
    cstruct_format = '%Y-%m-%dT%H:%M'
    pstruct_format = '%Y-%m-%d %H:%M'

    def serialize(self, field, cstruct, **kw):
        if cstruct in (c.null, None):
            cstruct = ''
        if cstruct:
            try:
                dt = datetime.datetime.strptime(cstruct, self.cstruct_format)
                cstruct = dt.strftime(self.pstruct_format)  # for the template
            except ValueError:
                cstruct = ''
        values = self.get_template_values(field, cstruct, kw)
        return field.renderer(self.template, **values)

    def deserialize(self, field, pstruct):
        if pstruct is c.null:
            return c.null
        else:
            try:
                dt = datetime.datetime.strptime(pstruct, self.pstruct_format)
            except ValueError:
                return c.null
            return dt.strftime(self.cstruct_format)


class SubmissionsAdminSchema(c.MappingSchema):
    @c.deferred
    def graders_widget(node, kw):
        values = [(g.id, g.name) for g in kw['graders']]
        return deform.widget.SelectWidget(values=values)

    @c.deferred
    def grader_validator(node, kw):
        ids = [g.id for g in kw['graders']]
        return c.OneOf(ids)

    first_deadline_override = c.SchemaNode(DateTime(),
                                           title="First deadline extension",
                                           widget=DateTimeInput(), missing=None)
    final_deadline_override = c.SchemaNode(DateTime(),
                                           title="Final deadline extension",
                                           widget=DateTimeInput(), missing=None)

    grader_id = c.SchemaNode(c.String(), 
                             widget=graders_widget,
                             validator=grader_validator,
                             title='Grader')


class GraderNotificationsSchema(c.MappingSchema):
    pass
