"""Defines default settings for Fire instances."""
import os
import re

from pyramid.config import Registry

import logging
log = logging.getLogger(__name__)

static_settings = {}  # TODO: is there a built-in global object for settings from the config file?


class FireConfigException(Exception):
    pass


def initialize(settings):
    """Set defaults for fire.* settings from .ini file and check that
    required settings are present. Initializes other default settings."""

    def cfg(key, default=None, required=False, match=None, convert_to=None):
        v = settings.get('fire.' + key, None)
        if v is None:
            if required:
                raise FireConfigException("Required setting fire.%s is not defined." % key)
            settings['fire.' + key] = default
            return
        if match is not None:
            if not re.match(match, v):
                raise FireConfigException("The setting fire.%s does not validate." % key)
        if convert_to is not None:
            settings['fire.' + key] = convert_to(v)

    # Everything will be served under this. 
    # IMPORTANT: Make sure all cookies have this path, or they will be shared
    #            between courses.
    cfg('course_slug', required=True, match=r'[a-z0-9_-]+')
    cfg('course_prefix', required=True, match=r'[a-z0-9_-]+(/[a-z0-9_-]+)*')
    cfg('auth_tkt_secret', required=True)
    cfg('data_dir', required=True)
    cfg('login_lifetime', default=86400, convert_to=int)
    cfg('debug', default=False)

    # Set derived settings
    slug = settings['fire.course_slug']
    prefix = settings['fire.course_prefix']
    data_dir = settings['fire.data_dir']
    settings.update({
        'sqlalchemy.url': 'sqlite:///' + os.path.join(data_dir, 'db.sqlite'),

        'session.type': 'file',
        'session.data_dir': os.path.join(data_dir, 'sessions/data'),
        'session.lock_dir': os.path.join(data_dir, 'sessions/lock'),
        'session.key': 'sess_tkt_' + slug,
        'session.cookie_on_exception': False,
        'session.cookie_path': '/' + prefix,
        'session.httponly': True
    })

    global static_settings
    static_settings = settings


def get(request, key):
    """A convenience method for fetching fire.* settings.

    ``request`` may be a request object or a configurationr, anything
        that stores the registry as a ``registry`` attribute.
    """
    if isinstance(request, Registry):
        reg = request
    elif hasattr(request, 'registry'):
        reg = request.registry
    else:
        raise ValueError('fire.settings.get did not receive a request, configurator or request.')
    return reg.settings['fire.' + key]


def get_static(key):
    return static_settings['fire.' + key]
