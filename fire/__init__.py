from pyramid.config import Configurator


def main(global_config, **settings_file):
    """Fire's main app factory. Performs Pyramid initialization
    and configuration of the Fire app."""

    import logging.config
    logging.config.fileConfig(global_config['__file__'])

    from . import (settings, security, routes, templating, models)

    # This must happen before constructing the configurator.
    # Triggers validation of fire.* settings and sets derived
    # settings such as sqlalchemy.* etc.
    settings.initialize(settings_file)

    config = Configurator(settings=settings_file, 
                          route_prefix=('/' + settings_file['fire.course_prefix']))

    # Include third party libraries
    config.include('pyramid_beaker')    # Sessions and caching
    config.include('pyramid_tm')        # Transaction management
    config.include('pyramid_mailer')    # Outgoing email

    # Include local subsystems
    # When a module is included in config, Pyramid looks for a callable
    # named ``includeme`` in that module, and passes it the configurator object.
    config.include(security)
    config.include(routes)
    config.include(templating)
    config.include(models)

    config.scan(ignore='fire.tests')
    return config.make_wsgi_app()
