define("views/admin_graders", ['require', 'jquery'], function(require, $) {
  $(function() {
  return $('#grader-assignment .weight').change(function() {
    var grader, graders, lab, total;
    lab = $(this).parent().data('lab');
    grader = $(this).parent().data('grader');
    graders = $('#grader-assignment td[data-lab=' + lab + ']');
    total = 0;
    graders.each(function() {
      return total += parseInt($(this).find("input").val());
    });
    if (total > 0) {
      $('#lab-th-' + lab).removeClass('understaffed');
      graders.removeClass('understaffed');
    } else {
      $('#lab-th-' + lab).addClass('understaffed');
      graders.addClass('understaffed');
    }
    return graders.each(function() {
      var input, percentage, weight;
      input = $(this).find("input");
      weight = parseFloat(input.val());
      percentage = total === 0 ? 0 : Math.floor(100 * weight / total);
      $(this).val(weight);
      $(this).find("label").html(percentage);
      if (weight > 0) {
        return input.removeClass('unassigned');
      } else {
        return input.addClass('unassigned');
      }
    });
  });
});

});
define("views/grader_lab", ['require', 'jquery'], function(require, $) {
  $(function() {
  var activetab, hash, updateDefaultRows, updateHiddenRows;
  hash = window.location.hash;
  if (hash.length) {
    activetab = $('#labtabs a[href="' + hash + '"]');
  } else {
    activetab = $("#labtabs a:first");
  }
  activetab.tab("show");
  updateDefaultRows = function() {
    return $('table.table tbody:has(tr.default-row)').each(function(idx, tb) {
      var empty;
      empty = $('tr:visible:not(.default-row)', tb).length === 0;
      return $('tr.default-row', tb).toggle(empty);
    });
  };
  updateDefaultRows();
  updateHiddenRows = function() {
    var show;
    show = $('#show-all-graders').hasClass('active');
    $('tr.other-grader').toggle(show);
    return updateDefaultRows();
  };
  $('#labtabs a').click(function() {
    return setTimeout(updateDefaultRows, 0);
  });
  return $('#show-all-graders').click(function() {
    return setTimeout(updateHiddenRows, 0);
  });
});

});
define("views/grader_main", ['require', 'jquery'], function(require, $) {
  var SubmissionsGraph;

SubmissionsGraph = (function() {
  function SubmissionsGraph(canvas) {
    this.canvas = canvas;
    this.url = $(this.canvas).data('source');
    this.colorAll = $(this.canvas).data('color-all');
    this.colorMe = $(this.canvas).data('color-me');
    this.context = this.canvas.getContext("2d");
    console.log("Drawing bars from " + this.url + " with colors " + this.colorAll + " and " + this.colorMe);
    this.refresh();
  }

  SubmissionsGraph.prototype.barWidth = 12;

  SubmissionsGraph.prototype.pxPerUnit = 5;

  SubmissionsGraph.prototype.drawBar = function(index, value) {
    return this.context.fillRect(this.canvas.width - (index + 1) * (this.barWidth + 1), this.canvas.height - value * this.pxPerUnit, this.barWidth, value * this.pxPerUnit);
  };

  SubmissionsGraph.prototype.draw = function(data) {
    var count, i, j, k, len, len1, ref, ref1, results;
    this.context.fillStyle = this.colorAll;
    ref = data.all;
    for (i = j = 0, len = ref.length; j < len; i = ++j) {
      count = ref[i];
      this.drawBar(i, count);
    }
    this.context.fillStyle = this.colorMe;
    ref1 = data.me;
    results = [];
    for (i = k = 0, len1 = ref1.length; k < len1; i = ++k) {
      count = ref1[i];
      results.push(this.drawBar(i, count));
    }
    return results;
  };

  SubmissionsGraph.prototype.refresh = function() {
    return $.ajax({
      url: this.url,
      dataType: "json",
      success: $.proxy(this.draw, this)
    });
  };

  return SubmissionsGraph;

})();

$(function() {
  var cva, j, len, ref, results;
  console.log("abcd");
  ref = $(".bars");
  results = [];
  for (j = 0, len = ref.length; j < len; j++) {
    cva = ref[j];
    results.push(new SubmissionsGraph(cva));
  }
  return results;
});

});
define("views/grader_physical_overview", ['require', 'jquery'], function(require, $) {
  $(function() {
  $('.register-button').click(function() {
    var row, submitter;
    $('tr').removeClass('info');
    $('.register-button').show().next().hide();
    row = $(this).closest('tr');
    row.addClass('info');
    $(this).hide().next().show();
    return submitter = row.data('submitter');
  });
  return $('.confirm-button').click(function() {
    var btn, form, row;
    btn = $(this);
    row = btn.closest('tr');
    form = $('#register-form');
    $('[name=submitter_id]', form).val(row.data('submitter'));
    btn.button('loading');
    return $.post(form.attr('action'), form.serialize(), function(data) {
      console.log(data);
      row.removeClass('info');
      if (data.error) {
        return btn.parent().html("<span class='text-error'>Error: " + data.error + "</span>");
      } else {
        return btn.parent().html("<span class='text-success'>Registered!</span>");
      }
    });
  });
});

});
define("views/grader_roster", ['require', 'jquery'], function(require, $) {
  $(function() {
  return $('td .icon').tooltip();
});

});
define("views/grader_submissions", ['require', 'jquery'], function(require, $) {
  $(function() {
  var autosave, autosave_interval, autosave_lastval, autosave_ongoing, autosave_time, form;
  $('[data-toggle=tooltip]').tooltip();
  $('.fileaction').each(function() {
    var $this, filename, subid;
    $this = $(this);
    subid = $this.closest('.submission').data('submission');
    filename = $this.closest('tr').data('filename');
    switch ($this.data('action')) {
      case "download":
        return $this.click(function() {
          return window.location.href += "/" + subid + "/files/" + filename + "?dl=1";
        });
      case "browse":
        return $this.attr('href', window.location.href + "/" + subid + "/files/" + filename);
    }
  });
  form = $('#review-form');
  if (form.length) {
    autosave_interval = 3000;
    autosave_lastval = form.serialize();
    autosave_time = -1;
    autosave_ongoing = false;
    autosave = function() {
      var newdata, now;
      newdata = form.serialize();
      if (!autosave_ongoing && newdata !== autosave_lastval) {
        now = new Date();
        if (now.getTime() - autosave_time > autosave_interval) {
          $.post(form.data('autosave-url'), newdata, function(data) {
            if (data.error) {
              return $('#submission-statusbar', form).text('Autosave failed: ' + data.error);
            } else {
              autosave_lastval = newdata;
              autosave_time = now.getTime();
              return $('#submission-statusbar', form).text('Autosaved at ' + now.toLocaleTimeString());
            }
          });
        }
      }
      return setTimeout(autosave, autosave_interval);
    };
    return autosave();
  }
});

});
define("views/login", ['require', 'jquery'], function(require, $) {
  $(function() {
  var resetpwd_btn, signup_btn;
  $('#loginform > input[name=email]').focus();
  signup_btn = $('#signup-btn');
  signup_btn.click(function() {
    $('#signup-form-row').collapse('show');
    signup_btn.prop('disabled', true);
    $('#loginform input').prop('disabled', true);
    $('#loginform button').prop('disabled', true);
    return $('#signupform input[name=signup_email]').focus();
  });
  if (window.location.hash === "#register") {
    signup_btn.click();
    $('#signupform input[name=signup_email]').focus();
  }
  resetpwd_btn = $('#resetpwd-btn');
  return resetpwd_btn.click(function() {
    $('#reset-password-form-row').collapse('show');
    $('#signup-btn').prop('disabled', true);
    $('#loginform input').prop('disabled', true);
    $('#loginform button').prop('disabled', true);
    return $('#reset-password-form-row input[name=reset_email]').focus();
  });
});

});
define("views/student_lab", ["jquery", "jquery.fileupload"], function($) {
  return $(function() {
    var activeUploads, setupFileActions;
    $('[data-toggle=tooltip]').tooltip();
    activeUploads = 0;
    $("#fileupload").fileupload({
      dataType: "json",
      add: function(e, data) {
        ++activeUploads;
        $('#submit-button').button('loading');
        return data.submit();
      },
      submit: function(e, data) {
        var td, tr;
        tr = $('<tr/>');
        td = $('<td class="filename"/>');
        td.innerHTML = data.files[0].name;
        tr.append(td);
        tr.append('<td colspan="4" class="message"><div class="progress"><div class="bar" style="width: 0%;"></div></div></td>');
        data.tableRow = tr;
        return $('#filelisting tbody').append(tr);
      },
      done: function(e, data) {
        var newrow;
        if (data.result.error) {
          return $('.message', data.tableRow).addClass('message-error').text(data.result.error);
        } else {
          newrow = $(data.result.tablerow);
          setupFileActions(newrow);
          $(data.tableRow).replaceWith(newrow);
          if (--activeUploads === 0) {
            return $('#submit-button').button('reset');
          }
        }
      },
      fail: function(e, data) {
        return $('.message', data.tableRow).addClass('message-error').text("Upload failed: " + data.jqXHR.statusText);
      },
      progress: function(e, data) {
        var bar, progress;
        progress = parseInt(data.loaded / data.total * 100, 10);
        bar = $(".progress .bar", data.tableRow).css('width', progress + '%');
        if (progress >= 95) {
          return bar.addClass("progress-striped active");
        }
      }
    });
    $('#browse-button').click(function() {
      return $('#fileupload').click();
    });
    $('.modal-cancel').click(function() {
      return $(this).closest('.modal').modal('hide');
    });
    setupFileActions = function(tr) {
      var $tr;
      $tr = $(tr);
      return $('.fileaction', tr).click(function() {
        var action, filename, mdl, subid;
        action = $(this).data('action');
        filename = $tr.data('filename');
        switch (action) {
          case "delete":
            mdl = $('#modal-confirm-delete');
            mdl.find('#confirm-delete-filename').text(filename);
            mdl.find('#confirm-delete-button').unbind('click').click(function() {
              var url;
              mdl.modal('hide');
              url = $(this).data('url');
              return $.post(url, filename, function(data) {
                var tbody;
                if (data.error) {
                  return alert(data.error);
                } else {
                  tbody = $tr.parent();
                  return $tr.slideUp(function() {
                    return $tr.remove();
                  });
                }
              });
            });
            mdl.modal();
            break;
          case "download":
            subid = $tr.closest('.submission').data('submission');
            return window.location.href += "/submissions/" + subid + "/files/" + filename + "?dl=1";
        }
      });
    };
    $('.filelisting-5col tr').each(function(idx, tr) {
      return setupFileActions(tr);
    });
    return $('#submit-button').click(function() {
      var mdl, p;
      mdl = $('#modal-confirm-submit');
      $('input[name=comment]').val($('#submission-comment').val());
      p = $('#physical_submission');
      if (p.length && p.is(':checked')) {
        $('input[name=physical]').val('yes');
      }
      return mdl.modal();
    });
  });
});
define("views/student_main", ['require', 'jquery'], function(require, $) {
  $(function() {
  $('.lab h2').click(function() {
    return $('a', this).click();
  });
  $('a[data-reveal]').click(function() {
    var $this;
    $this = $(this);
    return $this.replaceWith("<code>" + $this.data('reveal') + "</code>");
  });
  $('#modal-newgroup input[name=joincode]').keyup(function() {
    if ($(this).val().length > 0) {
      return $('#confirm-newgroup').text('Join group');
    } else {
      return $('#confirm-newgroup').text('Create group');
    }
  });
  $('#confirm-newgroup').click(function() {
    return $('#modal-newgroup form').submit();
  });
  return $('#confirm-leavegroup').click(function() {
    if (confirm("Are you sure you want to leave the group?")) {
      return $('#leavegroup form').submit();
    }
  });
});

});
