# Templating setup, Genshi for now
from pyramid.events import subscriber
from pyramid.events import BeforeRender
from translationstring import TranslationString

import genshi
import pyramid_chameleon
import pyramid_genshi

from datetime import datetime, timedelta

from . import settings
from .models import Submission, Settings


def includeme(config):
    config.add_renderer('.html', renderer_factory)
    config.add_renderer('genshistream', render_stream)
    config.add_request_method(flash_fire, 'flash_fire')


def renderer_factory(info):
    return pyramid_chameleon.renderer.template_renderer_factory(info, pyramid_genshi.GenshiTemplateRenderer)


def render_stream(info):
    def render(stream, system):
        """Just renders a Genshi stream as html. Useful as a
        renderer for view functions that return Genshi streams,
        e.g. forms"""
        return stream.render('html')
    return render


@subscriber(BeforeRender)
def add_renderer_globals(event):
    request = event['request']
    user_info = request.user.safe_info if request.user is not None else None

    import pkg_resources
    version = pkg_resources.require('fire')[0].version
    event.update(
        debug=settings.get(request, 'debug'),
        url=request.route_path,
        user=user_info,   # Use this when possible, to prevent revealing info by accident
        user_object=request.user,   # Avoid using this in templates, use user if possible
        user_stat=request.user.stat() if request.user else {},
        SubmissionStatuses=Submission.Statuses,
        utils=_renderer_utilities,
        fire_version=version,
        course_name=Settings.get('course_name'),
        course_url=Settings.get('course_url'),
        course_slug=settings.get(request, 'course_slug'),
        course_prefix=settings.get(request, 'course_prefix'),
        url_host=settings.get(request, 'url_host'),
        pop_flash=request.session.pop_flash,
        HTML=genshi.HTML
    )


def flash_fire(request, type='info', title='', msg='', queue='', allow_duplicate=True):
    request.session.flash({'type': type, 'title': title, 'msg': msg}, queue=queue, allow_duplicate=allow_duplicate)


def renderer_utility(fun):
    """A decorator that makes its function available to renderers
    under utils.<function_name>"""
    _renderer_utilities[fun.func_name] = fun
    return fun
_renderer_utilities = dict()


# Generic renderer utilities
@renderer_utility
def now():
    return datetime.now()


@renderer_utility
def relative_time(dt_or_delta, shortfmt=False, nosign=False):
    """
    Returns a human readable string of a time interval.

    If the first parameter is a datetime, it takes the interval
    from the current time, otherwise it assumes it is a timedelta.

    If shortfmt is true (default is false), the string is rendered
    with a +/- signs instead of the words "in"/"ago", while if nosign
    is true, the sign is discarded.
    """
    fmts = (('%.0fw', '%dd', '%dh %dm', '%dm') if shortfmt else 
            ('%.0f weeks', '%d days', '%d hrs %d min', '%d min'))
    delta = False
    if isinstance(dt_or_delta, datetime):
        iv = dt_or_delta - datetime.now()  # relative to now
    else:
        assert isinstance(dt_or_delta, timedelta)
        iv = dt_or_delta
        delta = True
    negative = iv.total_seconds() < 0
    if negative:
        iv = -iv
    if iv.days > 28:   # Until 2 weeks show number of weeks
        s = fmts[0] % (iv.days / 7)
    elif iv.days > 1:  # Until 48 hours show number of days
        s = fmts[1] % iv.days
    else:
        h = 24 * iv.days + iv.seconds / 3600
        m = (iv.seconds % 3600) / 60
        s = (fmts[2] % (h, m)) if h > 0 else (fmts[3] % (m,))
    if nosign:
        return s
    if shortfmt:
        return ("-" if negative else "+") + s
    elif delta:
        if negative:
            return s + " before"
        else:
            return "after " + s
    else:
        if negative:
            return s + " ago"
        else:
            return "in " + s


@renderer_utility
def format_size(size):
    if size < 1024:
        return "%d B" % size
    elif size < 1048576:
        return "%.1f kB" % (size / 1024.0)
    else:
        return "%.2f MB" % (size / 1048576.0)


@renderer_utility
def filelisting_row(file_info, show_delete=True, browsable=True):
    """
    This function builds a table row to display uploaded files.
    It takes as an argument a fire.filestore.FileMetadata object
    """
    t = genshi.builder.tag
    tr = t.tr(data_filename=file_info.name)
    filename = t.td(class_='filename')
    if file_info.browsable and browsable:
        filename.append(t.a(file_info.name, class_="fileaction", data_action='browse'))
        if file_info.sensitive:
            filename.append(t.span(class_="icon icon-warning-sign", data_toggle="tooltip",
                                   title="Will be opened as plain text"))
    else:
        filename.append(file_info.name)
    tr.append(filename)
    tr.append(t.td(format_size(file_info.size), class_='size'))
    tr.append(t.td(file_info.utime.strftime("%Y-%m-%d @ %H:%M"), class_='date'))

    actiontd = t.td(class_='actions')

    actiontd.append(t.a(data_action="download", class_="fileaction icon-download-alt", title="Download"))

    if show_delete:
        actiontd.append(t.a(data_action="delete", class_="fileaction icon-trash", title="Delete"))

    tr.append(actiontd)
    return tr


@renderer_utility
def passed(dt):
    """Returns true if dt has passed, with one minute grace period."""
    now = datetime.now()
    return False if dt is None else (now + timedelta(minutes=1)) > dt   # TODO: make the grace period configurable?


@renderer_utility
def translate(ts):
    return ts.interpolate() if isinstance(ts, TranslationString) else ts


@renderer_utility
def submissions_cmp(x, y):
    if x.submitted_date is None:
        if y.submitted_date is None:
            return cmp(x.id, y.id)
        else:
            return 1
    else:
        if y.submitted_date is None:
            return -1
        else:
            return cmp(x.submitted_date, y.submitted_date)


@renderer_utility
def graders_share(grader, assignments, percent_sign=True):
    total_weight = sum(assignments.values())
    weight = assignments.get(grader, 0)
    share = 0 if total_weight == 0 else int(round(100 * float(weight) / total_weight))
    return share if not percent_sign else "%d%%" % share
