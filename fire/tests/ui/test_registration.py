from unittest import TestCase
from time import sleep

from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import selenium.webdriver.support.expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import transaction

from . import LiveTestCase, mkurl
from fire.models import DBSession, CourseCode, EmailVerification

class TestRegistration(LiveTestCase):

    def test_registration(self):
        with transaction.manager:
            DBSession.add(CourseCode(code='XXX000'))
            DBSession.flush()

        self.driver.get(mkurl("login"))
        assert "Fire" in self.driver.title

        self.driver.find_element_by_id("signup-btn").click()
        field = self.driver.find_element_by_name("signup_email")
        field.send_keys("test@test.com")
        field.send_keys(Keys.RETURN)

        WebDriverWait(self.driver, 30).until(
                EC.text_to_be_present_in_element(
                      (By.ID, 'signupform'),
                      "Please visit verification URL sent by email"))
        code = DBSession.query(EmailVerification)\
            .filter(EmailVerification.email == 'test@test.com')\
            .all()[0].token
        self.driver.get(mkurl("login/new?token=%s" % code))

        self.driver.find_element_by_name("first_name").send_keys("John")
        self.driver.find_element_by_name("last_name").send_keys("Reese")
        self.driver.find_element_by_name("id_number").send_keys("0000000000")
        self.driver.find_element_by_name("password").send_keys("0"*12)
        self.driver.find_element_by_name("confirm_password").send_keys("0"*12)

        self.driver.find_element_by_id('signupformregister').click()

        self.driver.find_element_by_name("email").send_keys("test@test.com")
        self.driver.find_element_by_name("password").send_keys("0"*12)
        self.driver.get_screenshot_as_file('7a.png')
        self.driver.find_element_by_id('loginbtn').click()

        WebDriverWait(self.driver, 10).until(
                EC.text_to_be_present_in_element(
                      (By.TAG_NAME, 'h1'),
                      "Laboration overview"))
