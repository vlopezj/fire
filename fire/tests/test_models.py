from nose.tools import eq_, ok_

import unittest

from . import DBTestCase

from ..models import (
    User, Group, Lab, GroupLab, Submission, Grader, Student )
from .. import errors

class TestBase(DBTestCase):

    def test_base(self):
        u1,u2 = Student('userid'), Student('userid2')
        u1.first_name = 'John'
        u1.last_name  = 'Doe'
        u2.first_name = 'Mary'
        u2.last_name  = 'Poppins'
        self.save(u1, u2)

        eq_(u1.name, User.get('userid').name)

        us = User.get_all()
        eq_(len(us), 2)
        eq_(set(us), set([u1,u2]))


class TestCourseCode(DBTestCase):

    def test_init(self):
        from ..models import CourseCode
        cc = CourseCode( code = 'TST101' )
        self.assertEquals('TST101', cc.code)

    def test_init_not_blank(self):
        """ Test that the course code cannot be blank """
        from ..models import CourseCode
        cc = CourseCode( code = '' )
        with self.assertRaises(Exception):
            self.save( cc )

    def test_cannod_delete_code_if_student(self):
        """ Test that, if there is student with a particular course code,
        it's not possible to delete the code from the database.
        """
        from fire.models import CourseCode, Student
        cc = CourseCode( code = 'TST101' )
        self.save( cc )
        s = Student( 'test' )
        s.course = 'TST101'
        self.save( s )
        with self.assertRaises(Exception):
            self.session.delete( cc )
            self.session.flush()

class TestUser(DBTestCase):

    def test_pwproperty(self):
        u = Grader('userid') # Using grader here as we cannot save User in the db
        eq_(u.password, "")

        u.password = 'hunter2'
        eq_(u.password, "")

        self.save(u)

        eq_(User.check_password('userid', 'hunter2'), True)
        eq_(User.check_password('userid', 'nope!'), False)

        # Non-existing user
        eq_(User.check_password('foobar', 'dunmatter'), False)

    def test_repr(self):
        user = User("userid")
        self.assertEquals("<User userid>", repr(user))

    def test_unicode(self):
        """
        Test that the __unicode__ method provide usefull information
        """
        user = User("userid")
        user.first_name = "Luc"
        user.last_name = "Skywalker"
        user.email = "iamajedi@tatoin.eu"
        self.assertEquals("Luc Skywalker <iamajedi@tatoin.eu>", unicode(user))

    def test_init_mail(self):
        """
        Test that we can initialize a user using a mail address
        """
        user = User("test@test.com")
        self.assertEquals("test@test.com", user.email)

    def test_init_name_mail(self):
        """
        Test that we can initialize a user using a name <address> string
        of the same format that is accepted by mail servers.
        """
        user = User("Bob Bobsson <bob@test.com>")
        self.assertEquals("bob@test.com", user.email)
        self.assertEquals("Bob Bobsson", user.name)

    def test_print_empty_name(self):
        """
        Test that when a user has no name set, the ``name`` attribute returns none
        """
        user = User('test')
        self.assertIsNone(user.name)

    def test_cannot_save_user(self):
        """
        Test that it's not possible to save a simple User object
        I.e. users should be either grader or students.
        """
        user = User('test')
        with self.assertRaises(Exception):
            self.save(user)

class TestGrader(DBTestCase):

    def test_safe_info(self):
        g = Grader('John Doe <test@example.com>')
        self.assertEquals(g.safe_info,
            { 'course': None, 'id': 'test@example.com',
              'email': 'test@example.com', 'id_number': None,
              'name': 'John Doe'})


class TestStudent(DBTestCase):

    def setUp(self):
        super(TestStudent, self).setUp()
        from fire.models import CourseCode
        self.save( CourseCode( code = 'TST101' ) )

    def test_course_code(self):
        from ..models import Student
        s = Student('test')
        s.course = 'TST101'
        self.save(s)

    def test_invalid_course_code(self):
        from fire.models import Student
        s = Student('test')
        s.course = 'INV666'
        with self.assertRaises(Exception):
            self.save(s)


class TestLab(DBTestCase):

    def test_repr(self):
        l = Lab( title = 'Terraformation' )
        self.assertEquals( '<Lab: Terraformation>', repr(l))


class TestLab_graders(DBTestCase):
    """ Test the grader property of the Lab object """

    def setUp(self):
        super( self.__class__, self ).setUp()
        self.lab = Lab()
        self.save( self.lab )

    def testIsEmpty(self):
        self.assertEquals( self.lab.graders, [] )

    def testOneGrader(self):
        from fire.models import GraderAssignment, Grader
        ga = GraderAssignment( grader = Grader( 'test' ), lab = self.lab )
        self.save( ga )
        self.assertEquals( self.lab.graders, [ga] )

class TestLab_grader_assignment(DBTestCase):

    def setUp(self):
        super(self.__class__, self).setUp()
        self.lab = Lab()
        self.save( self.lab )

    def testIsEmpty( self ):
        self.assertEquals( self.lab.grader_assignment, {} )

    def testOneGrader(self):
        from fire.models import GraderAssignment, Grader
        ga = GraderAssignment( grader = Grader( 'test' ), lab = self.lab )
        self.save( ga )
        self.assertEquals( self.lab.grader_assignment, {'test': 1} )


class TestGraderAssignment_init(DBTestCase):

    def setUp(self):
        super(TestGraderAssignment_init, self).setUp()
        self.grader = Grader('test@test.com')
        self.save( self.grader )
        self.lab = Lab( id = 1 )
        self.save( self.lab )

    def testCanInitWithAllAttributes(self):
        #from fire.models import GraderAssignment, Lab, Grader
        from fire.models import GraderAssignment
        ga = GraderAssignment( lab = self.lab, grader = self.grader, weight = 2 )
        self.save( ga )
        self.assertEquals( self.lab, ga.lab)
        self.assertEquals( 1, ga.lab_id )
        self.assertEquals( self.grader, ga.grader )
        self.assertEquals( 'test@test.com', ga.grader_id )
        self.assertEquals( 2, ga.weight )

    def testCanLeaveOutWeight(self):
        from fire.models import GraderAssignment
        ga = GraderAssignment( lab = self.lab, grader = self.grader )
        self.save(ga)
        self.assertEquals( ga.weight, 1 )

    def testWeigthCannotBeZero(self):
        from fire.models import GraderAssignment
        with self.assertRaises( Exception ):
            self.save( GraderAssignment( lab = self.lab, grader = self.grader,
                                         weight = 0 ) )

class TestGroups(DBTestCase):

    def setUp(self):
        super(TestGroups, self).setUp()
        self.u1 = self._add_student('user1')
        self.u2 = self._add_student('user2')
        self.u3 = self._add_student('user3')
        self.u4 = self._add_student('user4')

    def test_create(self):
        u1 = self.u1
        g = Group(u1)
        self.save(g)
        self.assertEquals(g, u1.group )
        assert u1 in g.active_members

    def test_leave(self):
        """
        Test that user can leave a group, including the creator of the group.
        """
        group = Group(self.u1)
        self.save(group)
        group.add_user(self.u2)
        self.assertEqual( set( [ self.u1, self.u2 ] ),
                          set(group.active_members ))
        group.remove_user(self.u2)
        self.assertEqual( set( [ self.u1 ] ),
                          set(group.active_members ))
        group.remove_user(self.u1)
        self.assertEqual( set( [ ] ),
                          set(group.active_members ))

    @unittest.skip("There is no invite in the Group model")
    def test_joining(self):
        u1, u2 = self.u1, self.u2

        g = Group(u1)
        self.save(g)
        m = g.invite_user(u2)

        with self.assertRaises(ValueError): 
            g.remove_user(u2)  # Not a member yet

        g_unrelated = Group(u1)
        self.save(g_unrelated)

        # Make sure invite fails if used on a different group
        with self.assertRaises(ValueError): 
            g_unrelated.accept_invite(m)

        eq_(len(u2.group_invitations), 1)
        eq_(m, u2.group_invitations[0])
        eq_(m.group, g)
        eq_(m.invited_by, u1)
        assert u2 not in g.active_members
        with self.assertRaises(ValueError): 
            g.remove_user(u2)

        g.accept_invite(m)

        eq_(len(u2.group_invitations), 0)
        assert u2 in g.active_members
        with self.assertRaises(ValueError): 
            g.accept_invite(m)  # Already accepted

        g.remove_user(u2)

        eq_(len(u2.group_invitations), 0)
        assert u2 not in g.active_members
        with self.assertRaises(ValueError):
            g.accept_invite(m) # Invite should not be re-enabled
        with self.assertRaises(ValueError):
            g.remove_user(u2)  # Already removed

    def test_repr(self):
        group = Group(self.u1)
        self.save(group)
        self.assertEquals("<Group 1>", repr(group))

    def test_unicode(self):
        group = Group(self.u1)
        self.save(group)
        self.assertEquals(u"Group 1", unicode(group))


class TestSubmissions(DBTestCase):

    def setUp(self):
        from fire.models import GraderAssignment
        super(TestSubmissions, self).setUp()
        for name in ['alice', 'bob', 'charlie']:
            u = Student(name)
            self.save(u)
            setattr(self, name, u)
        grader = Grader('grader')
        self.save(grader)
        for ln in [1,2,3]:
            l = GroupLab()
            l.title = 'Lab %d' % ln
            l.graders.append( GraderAssignment( grader = grader ) )
            self.save(l)
            setattr(self, 'lab_%d' % ln, l)
        self.group_a = Group(self.alice)
        self.group_b = Group(self.bob)
        self.save(self.group_a, self.group_b)

    def test_create(self):
        g = self.group_a
        g.add_user(self.charlie)

        s1 = Submission(self.lab_1, g)
        assert s1.status is Submission.Statuses.new
        eq_(set(s1.submitters), set([self.alice, self.charlie]))
        eq_(s1.number, 1)

        # New members of the group should be added to non-submitted submission.
        g.add_user(self.bob)
        assert self.bob in set(g.active_members)
        assert self.bob in set(s1.submitters)

        # Charlie should not continue to be a submitter if he leaves the group
        s2 = Submission(self.lab_2, g)
        g.remove_user(self.charlie)
        assert self.charlie not in set(s1.submitters)

    @unittest.skip("Problems in the group models design")
    def test_doublesubmit(self):
        m = self.group_a.invite_user(self.charlie)
        self.group_a.accept_invite(m)

        s1 = Submission(self.lab_1, self.group_a)
        assert s1.status is Submission.Statuses.new
        eq_(s1.number, 1)

        self.group_a.remove_user(self.charlie)
        m = self.group_b.invite_user(self.charlie)
        self.group_b.accept_invite(m)
        
        with self.assertRaises(errors.DataInvariantException):
            s2 = Submission(self.lab_1, self.group_b)

        # reject first one, then a new submission should work
        s1.status = Submission.Statuses.rejected
        s2 = Submission(self.lab_1, self.group_b)

        # Now changing the status of the first one to an active status should fail
        with self.assertRaises(errors.DataInvariantException):
            s1.status = Submission.Statuses.accepted

    def test_withdraw_submitted(self):

      # Test simple withdraw
      s1 = Submission(self.lab_1, self.group_a)
      s1.submit()
      s1.withdraw()
      self.assertEquals( s1.status, Submission.Statuses.cancelled )

    def test_withdraw_new(self):
      # Withdrawing a non-submitted submission should do nothing
      s2 = Submission(self.lab_1, self.group_a)
      s2.withdraw()
      self.assertEquals( s2.status, Submission.Statuses.cancelled )

    def test_withdraw_accepted(self):
      """
      Withdrawing an accepted submission is impossible and should raise an
      exception
      """
      s = Submission(self.lab_1, self.group_a)
      s.post_review(True, 5, "", "")
      with self.assertRaises(ValueError):
          s.withdraw()
      self.assertEquals( s.status, Submission.Statuses.accepted )

    def test_withdraw_rejected(self):
      """
      Withdrawing an rejected submission is impossible and should raise an
      exception
      """
      s = Submission(self.lab_1, self.group_a)
      s.post_review(False, 5, "", "")
      with self.assertRaises(ValueError):
          s.withdraw()
      self.assertEquals( s.status, Submission.Statuses.rejected )
  

