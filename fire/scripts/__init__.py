import sys
import os
import textwrap

from pyramid.paster import (
    get_appsettings,
    setup_logging,
    )

from .. import settings as fire_settings

doc = \
"""Fire admin tool (Fire v.%(version)s)

Usage:
%(commands)s

Options:
  -h --help               Show this screen.
  --version               Show the version of Fire installed.
  -c FILE --config=FILE   Read configuration from file. [default: ~/fire.conf] 
"""

# globals initialized in fireadmin()
_commands = None
_cmd_name_map = None

def help(arguments):
    """help <command>
    Prints detailed help for <command>
    """
    cmd = _cmd_name_map[arguments['<command>']]
    print("Usage:\n  fireadmin " + cmd.__doc__)


def fireadmin():
    import sys
    from docopt import docopt
    import pkg_resources

    from . import initializedb, sampledata, settings, send_notifications

    global _commands, _cmd_name_map

    _commands = [ 
        settings.main,
        initializedb.main,
        sampledata.main,
        send_notifications.main,
        help,
        ]

    _cmd_name_map = { cmd.__doc__.split()[0]: cmd for cmd in _commands}

    cmds_doc = '\n'.join("  fireadmin [options] " + cmd.__doc__.split('\n')[0] for cmd in _commands)
    version = pkg_resources.require('fire')[0].version
    arguments = docopt(doc % {'version': version, 
                              'commands': cmds_doc}, version=version)

    # Find the selected command
    for name, cmd in _cmd_name_map.items():
        if arguments[name]:

            # Initialize settings
            config_uri = os.path.expanduser(arguments['--config'])
            setup_logging(config_uri)
            settings = get_appsettings(config_uri)
            fire_settings.initialize(settings)

            sys.exit(cmd(settings, arguments))
            break # in case cmd throws exception before sys.exit runs

def confirm_dangerous(msg, answer='yes'):
    tw = textwrap.wrap(textwrap.dedent(msg), 48)
    print ""
    print "   " + 56*"#"
    print "   ##############  WARNING! DANGER AHEAD!  ################"
    print "   " + 56*"#"
    for line in tw:
        print "   ### %-48s ###" % line
    print "   " + 56*"#"
    print ""
    inp = raw_input("Type 'confirm' (without quotes) to confirm: ")
    if inp.strip() == 'confirm':
        print "Confirmed, going ahead."
    else:
        print "Cancelled, exiting without any further action."
        sys.exit()
