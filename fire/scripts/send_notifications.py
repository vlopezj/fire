import sys
from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from ..models import *
from .. import mail, routes
import transaction


def main(settings, arguments):
    """send_notifications <grader> <period>

    Sends an email with a notification about new submissions, sent in a given period of time (in seconds)
    """
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    config = Configurator(settings=settings, route_prefix=('/' + settings['fire.course_prefix']))
    config.include('pyramid_mailer')
    config.include(routes)

    grader_id = arguments['<grader>']
    period = int(arguments['<period>'])

    grader = Grader.get(grader_id)

    if grader is None:
        sys.stderr.write("'%s' is not a valid grader email\n" % grader_id)
        return 1

    labs = Lab.get_active()

    def relevant(sub):
        delta = datetime.now() - sub.submitted_date
        return sub.grader is grader and delta.total_seconds() <= period

    pending_submissions = [(lab, lab.pending_submissions()) for lab in labs]
    relevant_submissions = [(lab, filter(relevant, subs)) for lab, subs in pending_submissions]
    submissions = [(lab, subs) for lab, subs in relevant_submissions if subs]

    if not submissions:
        print "No new submissions for %s" % grader_id
        return 0

    course_name = Settings.get('course_name')
    subject = "New submissions in %s" % course_name
    # TODO figure out how to access the proper routing from here
    route_url = lambda l, s: "%s%s/admin/labs/%s/submissions/%s" % (settings['fire.url_host'], config.route_prefix, l, s)
    mail.sendmail(config, [grader.email], subject,
                  'fire:templates/mails/notify_submissions.txt',
                  dict(url=route_url,
                       submissions=submissions,
                       grader=grader,
                       course_name=course_name))
    transaction.commit()

    total = sum(len(subs) for lab, subs in submissions)
    print "%d new submission(s) for %s" % (total, grader_id)
    return 0