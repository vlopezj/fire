import transaction

import getpass

from sqlalchemy import engine_from_config

from . import confirm_dangerous

from ..models import (
    DBSession,
    Base,
    Grader
    )

def main(settings, arguments):
    """initdb

    Initializes an empty database file.
    """

    _msg = """
    This command will attempt to create tables in the database configured in the configuration.
    DO NOT perform this operation on a live database.
    ONLY perform this operation on a fresh, *empty* data directory.
    """

    confirm_dangerous(_msg)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)

    while True:
        admin_setup = raw_input("Would you like to setup a course administrator? (y/n): ")
        if admin_setup == 'n':
            return
        elif admin_setup == 'y':
            break

    admin = Grader(raw_input("Email: "))
    admin.first_name = raw_input("First name: ")
    admin.last_name = raw_input("Last name: ")
    admin.password = getpass.getpass("Password: ")

    with transaction.manager:
        DBSession.add(admin)