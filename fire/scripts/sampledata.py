import transaction
from datetime import datetime, timedelta

from sqlalchemy import engine_from_config

from . import confirm_dangerous

from ..models import *

def main(settings, arguments):
    """sampledata

    Populates the database with sample data for debugging.
    """

    confirm_dangerous("""
    This command will insert sample data into the database configured
    to aid development.
    DO NOT use this command for live systems.
    """)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)

    session = DBSession()

    with transaction.manager:
      create_sample_data( session )


def create_sample_data( session ):
    session.add( CourseCode( code = 'TDA602' ) )
    session.add( CourseCode( code = 'TDA601' ) )
    session.add( CourseCode( code = 'DIT103' ) )
    session.add( CourseCode( code = 'DIT101' ) )
    Settings.set('course_name', 'Language-Based Security')

    gdrs = [ ('arnar.birgisson@chalmers.se', 'Arnar Birgisson', 'changeme')
           , ('andrei@chalmers.se', 'Andrei Sabelfeld', 'changeme')
           , ('jonas.magazinius@chalmers.se', 'Jonas Magazinius', 'changeme') 
           ]

    for email, name, pwd in gdrs:
        grader = Grader(email)
        grader.first_name,grader.last_name = name.split()
        grader.password = pwd
        session.add(grader)

    u1 = Student('arnarbi@gmail.com')
    u1.password = 'test'
    u1.first_name = 'Student'
    u1.last_name = 'Birgisson'
    u1.course = 'TDA602'
    u1.id_number = '0000000000'
    session.add(u1)

    u2 = Student('arnar@hvergi.net')
    u2.password = 'test'
    u2.first_name = 'Arnar'
    u2.last_name = 'Studentsson'
    u2.course = 'TDA602'
    u2.id_number = '0000000001'
    session.add(u2)

    labs = [ (1, 'Project proposal', 'proposal', datetime(2014, 4, 12, 23, 59, 59), 'andrei@chalmers.se')
           , (2, 'Eraser', 'eraser', datetime(2014, 4, 19, 23, 59, 59), 'arnar.birgisson@chalmers.se')
           , (3, 'VM Account', 'account', datetime(2014, 4, 26, 23, 59, 59), 'arnar.birgisson@chalmers.se')
           , (4, 'r00tshell', 'r00tshell', datetime(2014, 4, 26, 23, 59, 59), 'arnar.birgisson@chalmers.se')
           , (5, 'WebAppSec', 'webappsec', datetime(2014, 5, 3, 23, 59, 59), 'arnar.birgisson@chalmers.se')
           , (6, 'Project report', 'project', datetime(2014, 5, 24, 23, 59, 59), 'andrei@chalmers.se')
           ]

    for order, title, slug, deadline, grader in labs:
        l = GroupLab(title=title, slug=slug,
                first_deadline=deadline,
                final_deadline=deadline + timedelta(days=14),
                order=order)
        for g in Grader.get_all():
            l.graders.append( GraderAssignment( grader = g ) )
        l.max_members = 2
        l.min_members = 2
        session.add(l)

def fafl_samples():
    """Sample data based on the finite automata theory and formal languages course"""
    with transaction.manager:
        session.add( CourseCode( code = 'TMV027' ) )
        session.add( CourseCode( code = 'DIT321' ) )
        Settings.set('course_name', 'Finite automata theory and formal languages')

        u1 = Student('arnarbi@gmail.com')
        u1.email = 'arnarbi@gmail.com'
        u1.password = 'test'
        u1.name = 'Student Birgisson'
        u1.course = 'TMV027'
        u1.id_number = '0000000000'
        session.add(u1)

        u2 = Student('arnar@hvergi.net')
        u2.email = 'arnar@hvergi.net'
        u2.password = 'test'
        u2.name = 'Arnar Studentsson'
        u2.course = 'TMV027'
        u2.id_number = '0000000000'
        session.add(u2)

        grader = Student('arnar.birgisson@chalmers.se')
        grader.email = 'arnar.birgisson@chalmers.se'
        grader.password = 'test'
        grader.name = 'Arnar Birgisson'
        grader.role = User.Roles.grader
        session.add(grader)
        
        g1 = Group(u1)
        session.add(g1)
        i = g1.invite_user(u2)
        #g1.accept_invite(i)

        labs = [
                Lab(title='1 Formal proofs', slug="lab1", final_deadline=datetime(2013, 4, 11, 23, 59, 59), individual=True),
                Lab(title='2 DFA and NFA', slug="lab2", final_deadline=datetime(2013, 4, 18, 23, 59, 59), individual=True),
                Lab(title='3 Epsilon-NFA, RE', slug="lab3", final_deadline=datetime(2013, 4, 25, 23, 59, 59), individual=True),
                Lab(title='4 Regular languages', slug="lab4", final_deadline=datetime(2013, 5, 2, 23, 59, 59), individual=True),
                Lab(title='5 Context free grammars I', slug="lab5", final_deadline=datetime(2013, 5, 9, 23, 59, 59), individual=True),
                Lab(title='6 Context free grammars II', slug="lab6", final_deadline=datetime(2013, 5, 13, 23, 59, 59), individual=True),
                Lab(title='7 Context free grammars III', slug="lab7", final_deadline=datetime(2013, 5, 20, 23, 59, 59), individual=True),
               ]
        session.add_all(labs)

        for l in labs:
            l.grader_assignment = {'arnar.birgisson@chalmers.se': 1}
            l.allow_physical_submissions = True

        s = Submission(labs[0], u1)
        s.status = Submission.Statuses.submitted
        s.status = Submission.Statuses.rejected
        s.grader = grader
        s.grade = '1'
        s.review = 'Foobar'
        session.add(s)

        s = Submission(labs[0], u1)
        s.grader = grader
        session.add(s)

