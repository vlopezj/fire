$ ->
  $('.register-button').click ->
    # First reset all other rows
    $('tr').removeClass('info')
    $('.register-button').show().next().hide()

    # Show confirmation button
    row = $(this).closest('tr')
    row.addClass('info')
    $(this).hide().next().show()
    submitter = row.data('submitter')

  $('.confirm-button').click ->
    btn = $(this)
    row = btn.closest('tr')
    form = $('#register-form')
    $('[name=submitter_id]', form).val(row.data('submitter'))

    btn.button('loading')

    $.post form.attr('action'), form.serialize(), (data) ->
      console.log data
      row.removeClass('info')
      if data.error
        btn.parent().html("<span class='text-error'>Error: " + data.error + "</span>")
      else
        btn.parent().html("<span class='text-success'>Registered!</span>")
