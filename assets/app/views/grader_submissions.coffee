$ ->
  $('[data-toggle=tooltip]').tooltip()

  $('.fileaction').each ->
    $this = $(this)
    subid = $this.closest('.submission').data('submission')
    filename = $this.closest('tr').data('filename')
    switch $this.data('action')
      when "download"
        $this.click () ->
          window.location.href += "/" + subid + "/files/" + filename + "?dl=1"
      when "browse"
        $this.attr('href', window.location.href + "/" + subid + "/files/" + filename)

  form = $('#review-form')
  if form.length
    autosave_interval = 3000
    autosave_lastval = form.serialize()
    autosave_time = -1
    autosave_ongoing = false

    autosave = ->
      newdata = form.serialize()
      if !autosave_ongoing and newdata != autosave_lastval
        now = new Date()
        if now.getTime() - autosave_time > autosave_interval
          $.post form.data('autosave-url'), newdata, (data) ->
            if data.error
              $('#submission-statusbar', form).text('Autosave failed: ' + data.error)
            else
              autosave_lastval = newdata
              autosave_time = now.getTime()
              $('#submission-statusbar', form).text('Autosaved at ' + now.toLocaleTimeString())
      setTimeout autosave, autosave_interval

    autosave()