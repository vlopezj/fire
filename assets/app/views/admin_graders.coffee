$ ->
  $('#grader-assignment .weight').change ->
    lab = $(this).parent().data 'lab'
    grader = $(this).parent().data 'grader'

    graders = $('#grader-assignment td[data-lab=' + lab + ']')
    total = 0
    graders.each ->
        total += parseInt($(this).find("input").val())
    if total > 0
        $('#lab-th-' + lab).removeClass 'understaffed'
        graders.removeClass 'understaffed'
    else
        $('#lab-th-' + lab).addClass 'understaffed'
        graders.addClass 'understaffed'
    graders.each ->
        input = $(this).find("input")
        weight = parseFloat(input.val())
        percentage = if total == 0 then 0 else Math.floor(100 * weight / total)
        $(this).val weight
        $(this).find("label").html percentage
        if weight > 0
          input.removeClass 'unassigned'
        else
          input.addClass 'unassigned'