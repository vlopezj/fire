API documentation
*****************

This section documents the codebase, and is generated automatically from docstrings
inside the :mod:`fire` module.

:mod:`fire`
===========

.. automodule:: fire
   :members:

:mod:`fire.models`
------------------

.. automodule:: fire.models
   :members:
   :undoc-members:

:mod:`fire.routes`
------------------

.. automodule:: fire.routes
   :members:
   :undoc-members:

:mod:`fire.security`
---------------

.. automodule:: fire.security
   :members:
   :undoc-members:

:mod:`fire.templating`
----------------------

.. automodule:: fire.templating
   :members:
   :undoc-members:

:mod:`fire.views`
-----------------

.. automodule:: fire.views
   :members:
   :undoc-members:
