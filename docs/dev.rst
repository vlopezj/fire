Development
***********

This section describes how to set up a development environment to work on the code
behind Fire, and how to produce release versions.

Setup a development environment
===============================

Development sever
=================

Running tests
=============

Databse model
=============

.. image:: development/domain-model.svg
   :align: center

Security
========

Emails
======

Git branches
============

The development model used for fire development is a model introduced by
Vincent Driessen in `A successful Git branching model
<http://nvie.com/posts/a-successful-git-branching-model/>`_.

All work is done on the develop branch, or a feature branch (which is then
merged into develop). master should *always* be clean. To make a release, You
make a release branch form master, merge anything you want to from develop
(usually everything). Then you merge the release branch into master and tag a
release. Usually the only commit on the release branch is actually bumping
the version numbers. Have a look at the git tree view of the history.

Here is a graphical representation, courtesy Vincent Driessen (Creative Commons
BY-SA)

.. image:: development/git-branching-model.png
   :align: center
   :scale: 60%

Release
-------

This is how to make a release::

    git checkout -b release/VERSION develop
    ## bump version, commit
    git checkout master
    git merge --no-ff release/VERSION
    git tag -a VERSION

After that, you want to merge it in develop::

    git checkout develop
    git merge --no-ff release/VERSION

And delete the branch::

    git branch -d release/VERSION

